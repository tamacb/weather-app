import 'package:intl/intl.dart';

const String weatherUrl = 'https://ibnux.github.io/BMKG-importer/cuaca/';

final DateTime now = DateTime.now();
final DateFormat formatter = DateFormat('yyyy-MM-dd');
final String formatted = formatter.format(now);