// To parse this JSON data, do
//
//     final modelWeather = modelWeatherFromJson(jsonString);

import 'dart:convert';

List<ModelWeather> modelWeatherFromJson(String str) => List<ModelWeather>.from(json.decode(str).map((x) => ModelWeather.fromJson(x)));

class ModelWeather {
  ModelWeather({
    this.jamCuaca,
    this.kodeCuaca,
    this.cuaca,
    this.humidity,
    this.tempC,
    this.tempF,
  });

  DateTime jamCuaca;
  String kodeCuaca;
  String cuaca;
  String humidity;
  String tempC;
  String tempF;

  factory ModelWeather.fromJson(Map<String, dynamic> json) => ModelWeather(
    jamCuaca: DateTime.parse(json["jamCuaca"]),
    kodeCuaca: json["kodeCuaca"],
    cuaca: json["cuaca"],
    humidity: json["humidity"],
    tempC: json["tempC"],
    tempF: json["tempF"],
  );
}

