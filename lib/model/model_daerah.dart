import 'package:get/get.dart';

class DaerahModel {
  DaerahModel({
    String id,
    String propinsi,
    String kota,
    String kecamatan,
    String lat,
    String lon,
  }) {
    this.id = id;
    this.propinsi = propinsi;
    this.kota = kota;
    this.kecamatan = kecamatan;
    this.lat = lat;
    this.lon = lon;

  }

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _propinsi = RxString();
  set propinsi(String value) => _propinsi.value = value;
  String get propinsi => _propinsi.value;

  RxString _kota = RxString();
  set kota(String value) => _kota.value = value;
  String get kota => _kota.value;

  RxString _kecamatan = RxString();
  set kecamatan(String value) => _kecamatan.value = value;
  String get kecamatan => _kecamatan.value;

  RxString _lat = RxString();
  set lat(String value) => _lat.value = value;
  String get lat => _lat.value;

  RxString _lon = RxString();
  set lon(String value) => _lon.value = value;
  String get lon => _lon.value;
}
