import 'package:dayareka/view/details_page/details_page.dart';
import 'package:dayareka/view/home_rekaweather/home_page.dart';
import 'package:get/get.dart';

routes() => [
  GetPage(name: "/home", page: () => HomePage()),
  GetPage(name: "/details", page: () => DetailsPage()),
];