import 'package:dayareka/model/model_weather.dart';
import 'package:dayareka/repo/remoterepo/remote_repo.dart';
import 'package:get/get.dart';

class DetailsController extends GetxController {
  final RemoteServices _repository = RemoteServices();
  var isLoading = true.obs;
  var detailList = List<ModelWeather>().obs;

  @override
  void onInit() {
    fetchDetails();
    super.onInit();
  }

  void fetchDetails() async {
    try {
      isLoading(true);
      var weather = await _repository.getWeather(Get.parameters['iddaerah']);
      print("data id dari controller home ${Get.parameters['iddaerah']}");
      if (weather != null) {
        detailList.value = weather;
      }
    } finally {
      isLoading(false);
    }
  }
}