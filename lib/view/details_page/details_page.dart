import 'package:dayareka/constant/constant.dart';
import 'package:dayareka/view/details_controller/details_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailsPage extends StatefulWidget {
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  final DetailsController _detailsController = Get.put(DetailsController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => SafeArea(
          child: Scaffold(
            body: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: () => Get.back(),
                      ),
                      Expanded(
                        child: Text(
                          '${Get.parameters['kecamatan']} , ${Get.parameters['kota']}',
                          style: TextStyle(
                              fontFamily: 'avenir',
                              fontSize: 18,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                    ],
                  ),
                ),
                (_detailsController.detailList.isEmpty) ? LinearProgressIndicator(backgroundColor: Colors.blue) : Container(
                  height: MediaQuery.of(context).size.height * 0.843,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15)),
                      color: Colors.white),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Today'),
                            Text('Updated 24m ago'),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Cuaca hari ini'),
                            Text('$formatted'),
                          ],
                        ),
                      ),
                      Container(
                        height: 120.0,
                        color: Colors.white,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount:
                              _detailsController.detailList.take(1).length,
                          itemBuilder: (context, index) => Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 100.0,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(15),
                                      color: Colors.lightBlueAccent),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    children: [
                                      Text('Cuaca'),
                                      Text(
                                          '${_detailsController.detailList[index].cuaca ?? 'data kosong'}'),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 100.0,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(15),
                                      color: Colors.lightBlueAccent),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    children: [
                                      Text('Temperature C'),
                                      Text(
                                          '${_detailsController.detailList[index].tempC}'),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 100.0,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(15),
                                      color: Colors.lightBlueAccent),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    children: [
                                      Text('Temperature F'),
                                      Text(
                                          '${_detailsController.detailList[index].tempF}'),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 100.0,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(15),
                                      color: Colors.lightBlueAccent),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    children: [
                                      Text('Humidity'),
                                      Text(
                                          '${_detailsController.detailList[index].humidity}'),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.56,
                        child: ListView.builder(
                          itemCount: _detailsController.detailList.length,
                          itemBuilder: (context, index) => Container(
                            height: 80.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Flexible(
                                  flex: 1,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Icon(Icons.cloud),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                              '${_detailsController.detailList[index].cuaca}'),
                                          SizedBox(width: 20.0,),
                                          Text(
                                              '${_detailsController.detailList[index].tempC}'),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.center,
                                    children: [
                                      Text(
                                          '${_detailsController.detailList[index].jamCuaca}'),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
