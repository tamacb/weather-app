import 'package:dayareka/constant/constant.dart';
import 'package:dayareka/view/home_controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _valDaerah;
  String _idDaerah;

  final HomeController _homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Obx(() => Scaffold(
        body: Stack(
          children: [
            Container(
              child: Image.asset(
                'assets/bground.jpg',
                fit: BoxFit.cover,
              ),
              height: double.infinity,
              width: double.infinity,
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 218, left: 20.0),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                'WeatherX',
                                style: TextStyle(
                                    fontFamily: 'avenir',
                                    fontSize: 32,
                                    fontWeight: FontWeight.w900),
                              ),
                            ),
                            Container(
                              height: 40.0,
                              width: 40.0,
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), color: Colors.lightBlueAccent),
                              child: IconButton(
                                icon: Icon(Icons.search),
                                onPressed: () => Get.defaultDialog(
                                    title: 'Choose your area',
                                    titleStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                                    middleText: 'Text di tengah',
                                    backgroundColor: Colors.white,
                                    radius: 15,
                                    content: Container(
                                      height: 450.0,
                                      width: 250.0,
                                      child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: _homeController.daerahList.length,
                                        itemBuilder: (context, index)=> GestureDetector(
                                          onTap: ()=> Get.toNamed('/details?iddaerah=${_homeController.daerahList[index].id}'
                                              '&kecamatan=${_homeController.daerahList[index].kecamatan}'
                                              '&kota=${_homeController.daerahList[index].kota}'),
                                            child: ListTile(title: Text('${_homeController.daerahList[index].kecamatan}'),)),
                                      ),
                                    ),
                                    )
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                'your location',
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.bold),
                              ),
                              width: 80.0,
                              height: 15.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  color: Colors.lightBlueAccent),
                            ),
                            Text(
                              'Wonogiri',
                              style: TextStyle(
                                  fontSize: 40, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              'Kab. Wonogiri',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: _homeController.weatherList.take(1).length,
                  itemBuilder: (context, index)=> Container(
                    width: MediaQuery.of(context).size.width * 0.285,
                    height: MediaQuery.of(context).size.height * 0.285,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(40)),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Align(alignment: Alignment.centerLeft,child: Text('Update 12 minutes ago', style: TextStyle(
                                fontSize: 12, fontWeight: FontWeight.w600))),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text('${_homeController.weatherList[0].cuaca}', style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.bold)), Text('$formatted', style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold))],
                          ),
                          Container(
                            child: Expanded(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [
                                  Text('${_homeController.weatherList[0].tempC} C', style: TextStyle(
                                      fontSize: 80, fontWeight: FontWeight.bold)),
                                  Text('TAP TO MORE'),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        bottomNavigationBar: Container(
          height: 70.0,
          width: double.infinity,
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(flex: 1,child: Padding(
                padding: const EdgeInsets.only(left: 25.0),
                child: Container(
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.home_filled),
                        Text(
                          'home',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  width: 80.0,
                  height: 25.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Colors.lightBlueAccent),
                ),
              ),),
              Flexible(flex: 1,child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.settings, ),
                        Text(
                          'settings',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    width: 80.0,
                    height: 25.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.lightBlueAccent),
                  ),
                  Container(
                    child: Icon(Icons.refresh),
                    width: 30.0,
                    height: 30.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.lightBlueAccent),
                  ),
                ],
              ),)
            ],
          ),
        ),
      )),
    );
  }
}
