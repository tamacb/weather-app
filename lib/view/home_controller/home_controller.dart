import 'package:dayareka/model/model_daerah.dart';
import 'package:dayareka/model/model_weather.dart';
import 'package:dayareka/repo/localrepo/local_repo.dart';
import 'package:dayareka/repo/remoterepo/remote_repo.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final RemoteServices _repository = RemoteServices();
  RxList<DaerahModel> daerahList = RxList<DaerahModel>([]);
  RxList<ModelWeather> weatherList = RxList<ModelWeather>([]);

  String idDaerah;
  var isLoading = true.obs;

  loadDaerah() async {
    List<dynamic> dataDaerah = await loadJson(
      "assets/daerah.json",
    );
    daerahList.addAll(dataDaerah
        .map<DaerahModel>((daerah) => DaerahModel(
            id: daerah["id"],
            kecamatan: daerah["kecamatan"],
            propinsi: daerah["propinsi"],
            kota: daerah["kota"],
            lat: daerah["lat"],
            lon: daerah["lon"]))
        .toList());
  }

  var selectedItem = "501270".obs;

  void fetchWeather() async {
    try {
      isLoading(true);
      var weather = await _repository.getWeather(Get.parameters['iddaerah'] ?? selectedItem.value);
      print("data id dari controller home ${Get.parameters['iddaerah']}");
      if (weather != null) {
        weatherList.value = weather;
      }
    } finally {
      isLoading(false);
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    loadDaerah();
    fetchWeather();
  }
}
