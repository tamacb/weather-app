import 'package:dayareka/constant/constant.dart';
import 'package:dayareka/model/model_weather.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class RemoteServices extends GetxController {
  static var client = http.Client();

  Future<List<ModelWeather>> getWeather(String id) async {
    var response = await client.get(weatherUrl+"$id"+".json");
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return modelWeatherFromJson(jsonString);
    } else {
      return null;
    }
  }
}
